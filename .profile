# Defaults
export EDITOR="/usr/bin/nvim"
export TERMINAL="/usr/bin/alacritty"
export BROWSER="/usr/bin/qutebrowser"
export READER="/usr/bin/zathura"

export PATH="$PATH:$HOME/.local/bin/"

# Requeried for X11-application in containers
xhost +local:docker >/dev/null
