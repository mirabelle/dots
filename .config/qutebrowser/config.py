# Tab-close
config.unbind('d')
config.unbind('<Ctrl-w>')
config.bind('D','tab-close')

# Tab move
config.bind('J','tab-prev')
config.bind('K','tab-next')

# Settings
c.content.geolocation = False
c.scrolling.bar = False
c.tabs.background = True
c.tabs.width = '15%'
c.zoom.default = '125%'
c.session.lazy_restore = False

